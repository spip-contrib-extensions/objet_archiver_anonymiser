<?php
/**
 * Déclarations relatives à la base de données
 *
 * @plugin     objet_archiver_anonymiser
 * @copyright  2023
 * @author     Vincent CALLIES
 * @licence    MIT
 * @package    SPIP\Objet_archiver_anonymiser\Base
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Déclaration des alias de tables et filtres automatiques de champs
 */
function objet_archiver_anonymiser_declarer_tables_interfaces($interfaces) {
	$interfaces['table_des_tables']['spip_auteurs_archives'] = 'auteurs_archives';
	return $interfaces;
}

/**
 * Déclaration des tables principales objet
 *
 * @note 
 * la fonction traduire_statut_auteur() dans ecrire/inc/filtres_ecrire
 * n'utilise pas statut_titres, mais en attendant,
 * on met en oeuvre le nécessaire.
 */
function objet_archiver_anonymiser_declarer_tables_objets_sql($tables){

	// -- Permettre d'archiver et d'anonymiser les auteurs et les autrices

	// La définition des statuts de l'objet éditorial auteur est particulière.
	// Elle est redéfinie via la variable $GLOBALS['liste_des_statuts'],
	// depuis le fichier objet_archiver_anonymiser_fonctions.php

	// donner un titre au statut et une icône
	$tables['spip_auteurs']['statut_titres']['7archiver'] = 'objet_archiver_anonymiser:statut_archive';
	$tables['spip_auteurs']['statut_images']['7archiver'] = 'auteur-7archive-xx.svg';
	$tables['spip_auteurs']['statut_titres']['8anonymiser'] = 'objet_archiver_anonymiser:statut_anonymise';
	$tables['spip_auteurs']['statut_images']['8anonymiser'] = 'auteur-8anonymise-xx.svg';
	// definir l'anonymisation
	$tables['spip_auteurs']['champs_anonymises'] = [
		'nom' => 'crypt',
		'email' => 'void',
		'login' => 'void',
		'pass' => 'void',
		'lang' => 'void',
		'bio' => 'crypt',
		'nom_site' => 'crypt',
		'url_site' => 'crypt',
		'imessage' => 'crypt',
		'pgp' => 'crypt'
	];

	// -- Permettre d'archiver les articles

	// rajouter une ligne à la définition des statuts de l'objet éditorial
	$tables['spip_articles']['statut_textes_instituer']['archive'] = 'objet_archiver_anonymiser:statut_archive';
	// donner un titre au statut et une icône
	$tables['spip_articles']['statut_titres']['archive'] = 'objet_archiver_anonymiser:statut_archive';
	// définir l'ensemble du tableau statut_images, manquant
	$tables['spip_articles']['statut_images'] = [
		'prepa'    => 'puce-preparer-xx.svg',
		'prop'     => 'puce-proposer-xx.svg',
		'publie'   => 'puce-publier-xx.svg',
		'refuse'   => 'puce-refuser-xx.svg',
		'archive'  => 'puce-archiver2-xx.svg', // 2 pour contourner la puce du même nom de mailshot
		'poubelle' => 'puce-supprimer-xx.svg',
	];

/*	// -- Permettre d'anonymiser les articles

	// rajouter une ligne à la définition des statuts de l'objet éditorial
	$tables['spip_articles']['statut_textes_instituer']['anonymise'] = 'objet_archiver_anonymiser:statut_anonymise';
	// donner un titre au statut et une icône
	$tables['spip_articles']['statut_titres']['anonyme'] = 'objet_archiver_anonymiser:statut_anonymise';
	// rajouter une ligne à la définition des statut_images
	$tables['spip_articles']['statut_images']['anonyme'] = 'puce-anonymiser-xx.svg';

	// definir l'anonymisation
	$tables['spip_articles']['champs_anonymises'] = [
		'surtitre' => 'crypt', // un cryptage
		'titre' => '<:info_sans_titre:>', // un idiome de langue
		'soustitre' => 'crypt',
		'descriptif' => 'binary', // un cryptage en 0 et 1
		'nom_site' => 'anonymisé', // une chaine de texte
		'url_site' => 'void', // rien
		'chapo' => 'crypt',
		'texte' => 'crypt',
		'ps' => 'crypt',
		'date' => 'blank_date', // 0000-00-00 00:00:00
		'date_redac' => 'blank_date',
		'date_modif' => 'blank_date',
	]; */

	return $tables;
}

/*
 * Déclaration des tables principales non objet
 */
function objet_archiver_anonymiser_declarer_tables_principales($tables) {
	// d'autres déclaration ?
	$tables['spip_auteurs_archives'] = [
		'field' => [
			'id_auteur' => 'bigint(21) not null',
			'login' => 'VARCHAR(255) BINARY',
			'pass' => "tinytext DEFAULT '' NOT NULL",
			'date' => 'TIMESTAMP',
		],
		'key' => [
			'PRIMARY KEY' => 'id_auteur',
		],
	];
	return $tables;
}
