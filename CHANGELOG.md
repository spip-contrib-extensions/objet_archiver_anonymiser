# Changelog

## [Unreleased]

### CHANGED

- `lang/` adopter la syntaxe valide à partir de SPIP 4.1 pour les fichiers de langue afin de préparer la migration en SPIP v5

## 1.1.0 - 2024-02-13

### FIXED

Commit 28d42099 - fix conflit avec la puce de mailshot

### CHANGED

Commit 1e53a17af2 - anonyme comme statut plutot que anonymise.

## 1.0.0 - 2023-04-14
