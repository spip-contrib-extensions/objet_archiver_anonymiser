<?php
/**
 * Fichier proposant les traitements nécessaires à l'anonymisation d'un objet.
 *
 *   exemple avec fonction charger_fonction()
 *     ```
 *     $anonymiser_objet = charger_fonction('anonymiser_objet', 'inc');
 *     $anonymiser_objet($id_objet, $objet);
 *     ```
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Fonction d'anonymisation
 * 
 * Le traitement suite l'anonymisation aura lieu par un job, qui sera associé à l'objet,
 * afin que le job puisse être annulé.
 * Le délai avant l'anonymisation est de trois jours,
 * pour être en harmonie avec le délai habituel
 * de suppression d'un objet placé dans le statut `poubelle`
 * 
 * @param  int    $id_objet
 * @param  string $objet
 * @return array  $retours
 *                deux clés : `ok` boolean pour le succès, false pour l'echec 
 *                            `message` string pour l'explication
 */
function inc_anonymiser_objet_dist(int $id_objet, string $objet){
	$retours = [ 'ok' => false, 'message' => 'erreur', 'args' => ['id_objet' => $id_objet, 'objet' => $objet] ];

	// déclarer le job
	$time = strtotime('+ 3 days') + 60;
	$description = _T('objet_archiver_anonymiser:desc_anonym_objet', array('id' => $id_objet, 'obj' => $objet));
	if ($id_job = job_queue_add(
		'anonymiser_objet_job',
		$description,
		[$id_objet,$objet],
		'inc/',
		true,
		$time
	)){
		// afficher le job sur la page de contenu et permettre son annulation
		job_queue_link($id_job,['objet' => $objet, 'id_objet' => $id_objet]);
		$retours['ok'] = true;
		$retours['message'] = "Le job n°$id_job est lié avec succès à l'objet $objet n°$id_objet";
	}
	return $retours;
}
