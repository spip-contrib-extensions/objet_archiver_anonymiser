<?php
/**
 * Fichier proposant les traitements nécessaires au desarchivage d'un objet.
 *
 *   exemple avec fonction charger_fonction()
 *     ```
 *     $desarchiver_objet = charger_fonction('desarchiver_objet', 'inc');
 *     $desarchiver_objet($id_objet, $objet);
 *     ```
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Fonction de desarchivage
 *
 * Attention, il s'agit du traitement suite au changement de statut.
 * Le changement de statut se fait en amont.
 *
 * @param  int    $id_objet
 * @param  int    $objet
 * @return array  $retours
 *                deux clés : `ok` boolean pour le succès, false pour l'echec 
 *                            `message` string pour l'explication
 */

function inc_desarchiver_objet_dist(int $id_objet, string $objet){
	$retours = [ 'ok' => false, 'message' => 'erreur', 'args' => ['id_objet' => $id_objet] ];

	// on verifie s'il n'y a pas une fonction dédiée à l'objet
	if (find_in_path('inc/archiver_' . $objet . '.php') and $desarchiver_objet_precis = charger_fonction('desarchiver_' . $objet, 'inc')) {
		return $desarchiver_objet_precis($id_objet);
	} else {
		// pas de traitement, le changement de statut suffit
		$retours['ok'] = true;
		$retours['message'] = 'Le désarchivage s’est réalisé avec succès.';
	}
	return $retours;
}
