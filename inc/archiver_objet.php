<?php
/**
 * Fichier proposant les traitements nécessaires à l'archivage d'un objet.
 *
 *   exemple avec fonction charger_fonction()
 *     ```
 *     $archiver_objet = charger_fonction('archiver_objet', 'inc');
 *     $archiver_objet($id_objet, $objet);
 *     ```
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Fonction relative au traitement d'un archivage
 * 
 * Attention, il s'agit du traitement suite au changement de statut.
 * La fonction ne procède pas au changement de statut.
 *
 * @param  int    $id_objet
 * @return array  $retours
 *                deux clés : `ok` boolean pour le succès, false pour l'echec 
 *                            `message` string pour l'explication
 */

function inc_archiver_objet_dist(int $id_objet, string $objet){
	$retours = [ 'ok' => false, 'message' => 'erreur', 'args' => ['id_objet' => $id_objet, 'objet' => $objet] ];

	// on verifie s'il n'y a pas une fonction dédiée à l'objet
	if (find_in_path('inc/archiver_' . $objet . '.php') and $archiver_objet_precis = charger_fonction('archiver_' . $objet, 'inc')) {
		return $archiver_objet_precis($id_objet, $objet);
	} else {
		// pas de traitement. Le changement de statut en soit suffit.
		$retours['ok'] = true;
		$retours['message'] = 'L’archivage s’est réalisé avec succès.';
	}
	return $retours;
}
