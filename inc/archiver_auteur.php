<?php
/**
 * Fichier proposant les traitements nécessaires à l'archivage d'un auteur.
 *
 *   exemple avec fonction charger_fonction()
 *     ```
 *     $archiver_auteur = charger_fonction('archiver_auteur', 'inc');
 *     $archiver_auteur($id_auteur);
 *     ```
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Fonction d'archivage
 *
 * Le statut `7archiver` en lui-même empêche de trouver ou de voir l'auteur archivé
 * dans la liste des auteurs et l'empêcher également d'accéder 
 * à l'espace privé car le statut archivage est une sous-catégorie
 * du statut de visiteur.
 * L'auteur peut encore accéder à l'espace public restreint.
 * Pour l'en empêcher, il faut mettre son login et password à null.
 * Attention l'API Editer_objet n'est pas utilisée et l'opération consiste à supprimer l'accès.
 * 
 * @param  int    $id_auteur
 * @return array  $retours
 *                deux clés : `ok` boolean pour le succès, false pour l'echec 
 *                            `message` string pour l'explication
 */

function inc_archiver_auteur_dist(int $id_auteur){
	$retours = [ 'ok' => false, 'message' => 'erreur', 'args' => ['id_auteur' => $id_auteur] ];

	// on met de côté le login et pass
	$from = 'spip_auteurs'; // impérativement un string
	$select = 'login,pass';
	$where = [ 'id_auteur='.$id_auteur ];
	if ($garde = sql_fetsel($select, $from, $where)){
		$login = $garde['login'];
		$pass = $garde['pass'];
		$retours['args']['login'] = $login;
		$retours['args']['pass'] = $pass;
	} else {
		$retours['message'] = 'Impossible d’obtenir le login et pass.';
		return $retours;
	}

	// on conserve le login et le pass dans une table pour permettre un retour en arrière
	$date = date('Y-m-d H:i:s');
	$from = 'spip_auteurs_archives';
	$couples = [
		'id_auteur' => $id_auteur,
		'login' => $login,
		'pass' => $pass,
		'date' => $date,
	];
	if ($id = sql_insertq($from, $couples)){
		$retours['args']['archivage'] = $date;
	} else {
		$retours['message'] = 'Impossible de sauvegarder le login et pass.';
		return $retours;
	}

	// on supprime le login et le pass de la table des auteurs
	$from = 'spip_auteurs';
	$couples = [ 'login' => '', 'pass' => '' ];
	$where =  "id_auteur=$id_auteur";
	if (sql_updateq($from, $couples, $where)){
		$retours['ok'] = true;
		$retours['message'] = 'L’archivage s’est réalisé avec succès.';
	} else {
		$retours['message'] = 'Impossible de supprimer le login et pass.';
	}
	return $retours;
}
