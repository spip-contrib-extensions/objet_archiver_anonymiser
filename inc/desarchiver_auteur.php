<?php
/**
 * Fichier proposant les traitements nécessaires au desarchivage d'un auteur.
 *
 *   exemple avec fonction charger_fonction()
 *     ```
 *     $desarchiver_auteur = charger_fonction('desarchiver_auteur', 'inc');
 *     $desarchiver_auteur($id_auteur);
 *     ```
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Fonction de desarchivage
 *
 * 
 * @param  int    $id_auteur
 * @return array  $retours
 *                deux clés : `ok` boolean pour le succès, false pour l'echec 
 *                            `message` string pour l'explication
 */

function inc_desarchiver_auteur_dist(int $id_auteur){
	$retours = [ 'ok' => false, 'message' => 'erreur', 'args' => ['id_auteur' => $id_auteur] ];

	// on recupère le login et le pass dans la table de sauvegarde
	$date = date('Y-m-d H:i:s');
	$from = 'spip_auteurs_archives';
	$where = 'id_auteur=' . $id_auteur;
	$select = '*';
	if ($retrouve = sql_fetsel($select, $from, $where)){
		$retours['args'] = $retrouve;
		$login = $retrouve['login'];
		$pass = $retrouve['pass'];
	} else {
		$retours['message'] = 'Impossible de retrouver le login et pass.';
		return $retours;
	}

	// on rétablit le login et le pass dans la table des auteurs
	$from = 'spip_auteurs';
	$couples = [ 'login' => $login, 'pass' => $pass ];
	$where =  "id_auteur=$id_auteur";
	if (sql_updateq($from, $couples, $where)){
		$retours['ok'] = true;
		$retours['message'] = 'Le désarchivage s’est réalisé avec succès.';
	} else {
		$retours['message'] = 'Impossible de rétablir le login et pass.';
	}

	// on supprime les informations dans la table de sauvegarde
	if (sql_delete('spip_auteurs_archives', $where)){
		$retours['ok'] = true;
		$retours['message'] = 'Le désarchivage s’est réalisé avec succès.';
	} else {
		$retours['message'] = 'Impossible de supprimer le login et pass de la table de sauvegarde.';
	}

	return $retours;
}
