<?php
/**
 * Fichier proposant les traitements nécessaires à l'anonymisation d'un auteur.
 *
 *   exemple avec fonction charger_fonction()
 *     ```
 *     $anonymiser_objet_job = charger_fonction('anonymiser_objet_job', 'inc');
 *     $anonymiser_objet_job($id_objet, $objet);
 *     ```
 */

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) return;

/**
 * Fonction d'anonymisation
 * 
 * La fonction est conçue pour être appelée dans le cadre d'un job,
 * voir le log queue.log si nécessaire.
 *
 * @param  int    $id_objet
 * @return array  $retours
 *                deux clés : `ok` boolean pour le succès, false pour l'echec 
 *                            `message` string pour l'explication
 */

function inc_anonymiser_objet_job(int $id_objet, string $objet){
	$retours = [ 'ok' => false, 'message' => 'erreur', 'args' => ['id_objet' => $id_objet, 'objet' => $objet] ];

	// Connaitre les champs qu'il faut traiter
	if (include_spip('inc/filtres') and $champs_anonymises = objet_info($objet, 'champs_anonymises')){
		// connaitre la table de l'objet et son id_table_objet
		$id_table_objet = id_table_objet($objet);
		$table_objet = table_objet_sql($objet);
		// Charger les champs
		$from = $table_objet;
		$select = '*';
		$where =  "$id_table_objet=$id_objet";
		if ($champs = sql_fetsel($select, $from, $where)){
			// traiter les champs
			foreach ($champs_anonymises as $champ => $traitement) {
				if (isset($champs[$champ])){
					if ($traitement === 'void'){
						$champs[$champ] = '';
					} elseif ($traitement === 'crypt'){
						if ($champs[$champ]) {
							$champs[$champ] = crypt($champs[$champ], 'oaa');
						}
					} elseif ($traitement === 'binary'){
						if ($champs[$champ]) {
							$champs[$champ] = preg_replace(['([a-jA-J])','([j-zJ-Z])','([^0|1])'],['0','1','0'], $champs[$champ]);
						}
					} elseif ($traitement === 'blank_date'){
						if ($champs[$champ]) {
							$champs[$champ] = '0000-00-00 00:00:00';
						}
					} elseif (strlen($traitement)){
						if ($champs[$champ]) {
							if (function_exists('_T_ou_typo')) {
								$champs[$champ] = _T_ou_typo($traitement);
							} else {
								$champs[$champ] = $traitement;
							}
						}
					}
				}
			}
		}
		// enregistrer les champs
		if (sql_updateq($from, $champs, $where)){
			$retours['ok'] = true;
			$retours['message'] = 'L’anonymisation s’est réalisé avec succès.';
		} else {
			$retours['message'] = 'Impossible de procéder à l’anonymisation.';
		}
	} else {
		$retours['message'] = 'Pour procéder à l’anonymisation, il est nécessaire de déclarer les `champs_anonymises` lors de la déclaration de la table.';
	}

	return $retours;
}
