<?php
/**
 * Utilisations de pipelines par le plugin objet_archiver_anonymiser
 *
 * @plugin     objet_archiver_anonymiser
 * @copyright  2023
 * @author     Vincent CALLIES
 * @licence    MIT
 * @package    SPIP\Objet_archiver_anonymiser\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Le pipeline post_edition pour prendre en charge les effets du changement de statut
 * d'un objet archivé ou anonymisé. 
 *
 * Ce pipeline permet d’agir lors de l’édition d’un élément éditorial, 
 * lorsque l’utilisateur édite les champs ou change le statut de l’objet.
 * Il est appelé juste après l’enregistrement des données.
 *
 * Pour la création d'un nouvel objet (et non sa modification), 
 * le pipeline utile est `post_insertion`.
 * Un objet ne devant pas être créer dans un statut archivé ou anonymisé,
 * qui est normalement des statuts de fin de vie d'un objet,
 * ce pipeline n'est pas utilisé ; à vous de le mettre en oeuvre
 * si un usage particulier vous est nécessaire.
 *
 * @param array $flux
 *        Le contexte du pipeline sous forme d'un tableau comportant 2 clés :
 *          ['data'] les champs et leurs valeurs changées (pas celles qui n'ont pas changées).
 *          ['args'] les informations sur la table, l’objet édité, et l’action effectuée.
 * @return array
 *         Le contexte du pipeline modifié
 */
function objet_archiver_anonymiser_post_edition($flux) {
	if (isset($flux['args']['action']) and $action = $flux['args']['action']
		and isset($flux['args']['table']) and $objet = objet_type($flux['args']['table'])
		and isset($flux['args']['id_objet']) and $id = intval($flux['args']['id_objet'])
	){

		# modification du STATUT

		if ($action == 'instituer'
			and isset($flux['data']['statut']) and $nv_statut = $flux['data']['statut']
			and isset($flux['args']['statut_ancien']) and $ancien_statut = $flux['args']['statut_ancien']
		) {

			// les statuts particuliers des auteurs
			if ($objet === 'auteur') {
				if ($nv_statut === '7archive' and $ancien_statut != '7archive'){
					$archiver_auteur = charger_fonction('archiver_auteur', 'inc');
					$retours = $archiver_auteur($id);
					spip_log($retours, 'objet_archiver_anonymiser.' . _LOG_INFO_IMPORTANTE);
				}

				if ($nv_statut != '7archive' and $ancien_statut === '7archive'){
					$desarchiver_auteur = charger_fonction('desarchiver_auteur', 'inc');
					$retours = $desarchiver_auteur($id);
					spip_log($retours, 'objet_archiver_anonymiser.' . _LOG_INFO_IMPORTANTE);
				}

				if ($nv_statut === '8anonyme' and $ancien_statut != '8anonyme'){
					$anonymiser_objet = charger_fonction('anonymiser_objet', 'inc');
					$retours = $anonymiser_objet($id, $objet);
					spip_log($retours, 'objet_archiver_anonymiser.' . _LOG_INFO_IMPORTANTE);
				}
			} else {
				// les statuts d'autres objets SPIP
				if ($nv_statut === 'archive' and $ancien_statut != 'archive'){
					$archiver_objet = charger_fonction('archiver_objet', 'inc');
					$retours = $archiver_objet($id, $objet);
					spip_log($retours, 'objet_archiver_anonymiser.' . _LOG_INFO_IMPORTANTE);
				}
	
				if ($nv_statut != 'archive' and $ancien_statut === 'archive'){
					$desarchiver_objet = charger_fonction('desarchiver_objet', 'inc');
					$retours = $desarchiver_objet($id, $objet);
					spip_log($retours, 'objet_archiver_anonymiser.' . _LOG_INFO_IMPORTANTE);
				}
	
				if ($nv_statut === 'anonyme' and $ancien_statut != 'anonyme'){
					$anonymiser_objet = charger_fonction('anonymiser_objet', 'inc');
					$retours = $anonymiser_objet($id, $objet);
					spip_log($retours, 'objet_archiver_anonymiser.' . _LOG_INFO_IMPORTANTE);
				}
			}
		}
	}
	return $flux;
}