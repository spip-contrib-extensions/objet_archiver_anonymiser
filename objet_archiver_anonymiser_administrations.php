<?php
/**
 * Fichier gérant l'installation et désinstallation du plugin objet_archiver_anonymiser
 *
 * @plugin     objet_archiver_anonymiser
 * @copyright  2023
 * @author     Vincent CALLIES
 * @licence    MIT
 * @package    SPIP\Objet_archiver_anonymiser\Installation
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

 include_spip('base/upgrade');
 
/**
 * Fonction d'installation et de mise à jour du plugin auteur_archiver_anonymiser.
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @param string $version_cible
 *     Version du schéma de données dans ce plugin (déclaré dans paquet.xml)
 * @return void
**/
function objet_archiver_anonymiser_upgrade($nom_meta_base_version, $version_cible) {
	$maj = [];

	$maj['create'] = [
		['maj_tables', ['spip_auteurs_archives']]
	];

	// mettre à jour les statuts

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}


/**
 * Fonction de désinstallation du plugin
 *
 * @param string $nom_meta_base_version
 *     Nom de la meta informant de la version du schéma de données du plugin installé dans SPIP
 * @return void
 */
function objet_archiver_anonymiser_vider_tables($nom_meta_base_version) {
	// le retrait des nouveaux statuts d'un auteur
	if (isset($GLOBALS['liste_des_statuts']['objet_archiver_anonymiser:info_archive'])){
		unset($GLOBALS['liste_des_statuts']['objet_archiver_anonymiser:info_archive']);
	}
	if (isset($GLOBALS['liste_des_statuts']['objet_archiver_anonymiser:info_anonymise'])){
		unset($GLOBALS['liste_des_statuts']['objet_archiver_anonymiser:info_anonymise']);
	}

	// désarchivage des auteurs archivés
	$desarchiver_auteur = charger_fonction('desarchiver_auteur', 'inc');
	if ($resultats = sql_select('id_auteur', 'spip_auteurs', 'statut="7archive"')) {
		while ($res = sql_fetch($resultats)) {
			$desarchiver_auteur($res['id_auteur']);
		}
	}
	// bascule en `5poubelle` des auteurs qui renoncent au statut anonymiser.
	sql_update('spip_auteurs', ['statut' => sql_quote('5poubelle')], 'statut="8anonyme"');
	// bascule en `6forum` des auteurs qui renoncent au statut archiver.
	sql_update('spip_auteurs', ['statut' => sql_quote('6forum')], 'statut="7archive"');
	
	// bascule en `prepa` des articles qui renoncent aux statuts anonymiser et archiver.
	sql_update('spip_articles', ['statut' => sql_quote('prepa')], 'statut IN ("archive", "anonyme")');

	// destruction de la table de sauvegarde pour les auteurs archivés
	sql_drop_table('spip_auteurs_archives');

	# effacer_config("auteur_archiver_anonymiser");
	effacer_meta($nom_meta_base_version);
}