<?php
/**
 * Fonctions utiles au plugin objet_archiver_anonymiser
 *
 * Le fichier de fonctions est chargé au calcul des squelettes.
 * Si les fonctions ne sont utilisées que pour un seul squelette,
 * il est possible de déclarer un fichier homonyme au squelette, 
 * dans le même répertoire, en le suffixant de _fonctions.php. 
 * Le fichier de fonctions ne sera chargé que pour ce squelette. 
 *
 * @plugin     objet_archiver_anonymiser
 * @copyright  2023
 * @author     Vincent CALLIES
 * @licence    MIT
 * @package    SPIP\Objet_archiver_anonymiser\Fonctions
 *
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

// ajouter à la liste des statuts les deux nouveaux statuts d'un auteur
$GLOBALS['liste_des_statuts']['objet_archiver_anonymiser:info_archive'] = '7archive';
$GLOBALS['liste_des_statuts']['objet_archiver_anonymiser:info_anonymise'] = '8anonyme';
