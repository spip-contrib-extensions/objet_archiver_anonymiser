<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
	'aide_dev_description' => 'Ce plugin a pour but de permettre l’archivage et l’anonymisation des objets.',
	'aide_dev_nom' => 'objet_archiver_anonymiser',
	'aide_dev_slogan' => 'Permettre d’archiver ou anonymiser des objets...'
];
