<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
	'info_archive' => '> à archiver',
	'info_anonymise' => '> à anomymiser',
	'statut_archive' => 'archivé',
	'statut_anonymise' => 'anomymisé',
	'desc_anonym_objet' => 'Anonymisation de l’objet @obj@ n°@id@',
];